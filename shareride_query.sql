1) create database shareride;

2) use shareride;

3) show tables;
+--------------------+
| Tables_in_capstone |
+--------------------+
| admin              |
| bookings_data      |
| owner_message      |
| ride               |
| user               |
| vehicle_data       | 
| user_message       |
+--------------------+

4) desc admin;
+----------+--------------+------+-----+---------+-------+
| Field    | Type         | Null | Key | Default | Extra |
+----------+--------------+------+-----+---------+-------+
| email    | varchar(255) | NO   | PRI | NULL    |       |
| name     | varchar(255) | YES  |     | NULL    |       |
| password | varchar(255) | YES  |     | NULL    |       |
+----------+--------------+------+-----+---------+-------+


5) desc vehicle_data;
+------------+--------------+------+-----+---------+----------------+
| Field      | Type         | Null | Key | Default | Extra          |
+------------+--------------+------+-----+---------+----------------+
| id         | int          | NO   | PRI | NULL    | auto_increment |
| chargeinkm | double       | YES  |     | NULL    |                |
| members    | varchar(255) | YES  |     | NULL    |                |
| type       | int          | YES  |     | NULL    |                |
| name       | varchar(255) | YES  |     | NULL    |                |
+------------+--------------+------+-----+---------+----------------+


6) desc user;
+----------+--------------+------+-----+---------+-------+
| Field    | Type         | Null | Key | Default | Extra |
+----------+--------------+------+-----+---------+-------+
| email    | varchar(255) | NO   | PRI | NULL    |       |
| name     | varchar(255) | YES  |     | NULL    |       |
| password | varchar(255) | YES  |     | NULL    |       |
+----------+--------------+------+-----+---------+-------+


7) desc ride;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int          | NO   | PRI | NULL    | auto_increment |
| date_time   | datetime(6)  | YES  |     | NULL    |                |
| destination | varchar(255) | YES  |     | NULL    |                |
| source      | varchar(255) | YES  |     | NULL    |                |
| vehicle     | int          | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+


8) desc bookings_data;
+--------+--------------+------+-----+---------+----------------+
| Field  | Type         | Null | Key | Default | Extra          |
+--------+--------------+------+-----+---------+----------------+
| id     | int          | NO   | PRI | NULL    | auto_increment |
| date   | datetime(6)  | YES  |     | NULL    |                |
| status | int          | YES  |     | NULL    |                |
| ride   | int          | YES  | MUL | NULL    |                |
| user   | varchar(255) | YES  | MUL | NULL    |                |
+--------+--------------+------+-----+---------+----------------+


9) desc owner_message;
+---------+--------------+------+-----+---------+-------+
| Field   | Type         | Null | Key | Default | Extra |
+---------+--------------+------+-----+---------+-------+
| id      | varchar(255) | NO   | PRI | NULL    |       |
| message | varchar(255) | YES  |     | NULL    |       |
+---------+--------------+------+-----+---------+-------+

10)desc user_message;
+---------+--------------+------+-----+---------+-------+
| Field   | Type         | Null | Key | Default | Extra |
+---------+--------------+------+-----+---------+-------+
| id      | varchar(255) | NO   | PRI | NULL    |       |
| message | varchar(255) | YES  |     | NULL    |       |
+---------+--------------+------+-----+---------+-------+

11) select * from admin;
+-----------------+-------+----------+
| email           | name  | password |
+-----------------+-------+----------+
| admin@hcl.com   | admin | admin    |
+-----------------+-------+----------+

12) select * from user;
+------------------+--------+----------+
| email            | name   | password |
+------------------+--------+----------+
| user@gmail.com   | user   | user     |
| user1@gmail.com  | user1  | user1    |
| user2@gmail.com  | user2  | user2    |
+------------------+--------+----------+

13) select * from vehicle_data;
+----+------------+---------+------+--------+
| id | chargeinkm | members | type | name   |
+----+------------+---------+------+--------+
|  1 |        400 | 4       |    4 | BMW    |
|  2 |        300 | 2       |    2 | Pulsar |
|  3 |        600 | 1       |    2 | honda  |
+----+------------+---------+------+--------+

14) select * from bookings_data;
+----+----------------------------+--------+------+-----------------+
| id | date                       | status | ride | user            |
+----+----------------------------+--------+------+-----------------+
|  1 | 2022-11-30 16:23:29.333039 |      2 |    2 | user@gmail.com  |
+----+----------------------------+--------+------+-----------------+

15) select * from ride;
+----+----------------------------+-------------+---------+---------+
| id | date_time                  | destination | source  | vehicle |
+----+----------------------------+-------------+---------+---------+
|  1 | 2022-11-30 12:00:00.000000 | Madhapur    | Medchal |       1 |
+----+----------------------------+-------------+---------+---------+

16) select * from owner_message;
+-----------------+---------------------------+
| id              | message                   |
+-----------------+---------------------------+
| user@gmail.com  | your order booked         |
| user1@gmail.com | hii welcome to share ride |
| user2@gmail.com | hii sir                   |
+-----------------+---------------------------+

17) select * from user_message;
+-----------------+---------------------------------+
| id              | message                         |
+-----------------+---------------------------------+
| user@gmail.com  | is my request is booked or not? |
| user1@gmail.com | yes                             |
| user3@gmail.com | gfh                             |
+-----------------+---------------------------------+